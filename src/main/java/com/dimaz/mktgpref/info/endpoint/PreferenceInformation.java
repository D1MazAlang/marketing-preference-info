package com.dimaz.mktgpref.info.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.dimaz.mktgpref.info.model.Customer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1")
public class PreferenceInformation {

	public PreferenceInformation() {
		
	}
	
	RestTemplate restTemplate;
	
	@Autowired
	public PreferenceInformation(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@GetMapping("/mareting/preference/info/greet")
	public ResponseEntity<String> greetings() {
		return new ResponseEntity<String>("Welcome to Marketing Preference Info Microservice", HttpStatus.OK);
	}

	@GetMapping("/marketing/preference/info/{id}")
	public ResponseEntity<Customer> getMarketingPreference(@PathVariable(name = "id" , required = true) Long id) {
	
		log.info("Get Marketing Preference from Marketing Preference Registry ");
		
		String mktgPrefUrl = "http://localhost:9191/v1/marketing/preference/"+id;
		Customer cust = new Customer();
		
		try {
			cust = restTemplate.getForObject(mktgPrefUrl, Customer.class);
			
		}catch (HttpClientErrorException ex) {
			log.error("HTTP STATUS CODE: " + ex.getStatusCode());
			log.error(ex.getMessage());
			
			return new ResponseEntity<Customer>(cust, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Customer>(cust, HttpStatus.OK);

	}
	
}
