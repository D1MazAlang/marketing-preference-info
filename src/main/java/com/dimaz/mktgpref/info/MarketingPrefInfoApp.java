package com.dimaz.mktgpref.info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketingPrefInfoApp {

	public static void main(String[] args) {
		SpringApplication.run(MarketingPrefInfoApp.class, args);
	}

}
